If this is given
```
<num> :: "0" | "1"
<operand> :: "+" | "-"
<unary_operand> :: "!"
```

- Option#0


```text
<expression> :: <num>
                | <unary_operand> <num>
                | <num> <operand> <expression>
```

Cons: Two different branches with same starting point.
    If a num is read there are no way determinating which point is the correct unless
    the content is read forward, but it will cause some big shit

- Option#1

```text
<expression> :: [ <unaryOperand> ] <num> [ <operand> <expression> ]
```

# Upcoming features

- Expression recursion checks
- Expression validation for missing non-terminals
- Expression validation for unused non-terminals
- Pattern matching with HBNF expression


## Feature request: Postfix multiple

This would enable the following pattern:

```hbnf
<some_stuff> :: "a" <token>.many "b"
```

```hbnf
<some_stuff> :: "a" <token> * "b"
```

Where the Postfix multiple operand would expand the following rule:

```hbnf
<some_stuff> :: "a" <token_many> "b"

<token_many> :: <token> [ <token_many> ]
```

```
<T> *
===
<T_many> :: <T> [ <T> ]
```


```hbnf_grammar_test

<root> :: "a" "b"

####### Simple case 0

a b

####### Simple case 1

a b b b

```

Problems could occur in the following scenario:

<root> :: "a" "b" * "b"

The star would exhaust every "b", so it won't match anything

