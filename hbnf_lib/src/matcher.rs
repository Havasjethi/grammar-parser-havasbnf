use sql_parser::sql_recommendation::iterable_vec::IterableVec;

use crate::{
    input_tokeniser::{InputToken, InputTokeniser},
    types::*,
};

#[derive(Debug, Clone)]
pub struct Matcher {
    root: NonTerminal,
    non_terminals: Vec<NonTerminal>,
}

#[derive(Debug)]
pub struct MatcherResult {}

impl Matcher {
    pub fn new(root: NonTerminal, non_terminals: Vec<NonTerminal>) -> Self {
        Self {
            root,
            non_terminals,
        }
    }

    fn matches(&self, text_to_match: &str) -> Option<MatcherResult> {
        let tokeniser = InputTokeniser;
        let mut tokens = IterableVec::from(tokeniser.tokenise(text_to_match));

        let res = self.matches_(&self.root.value, &mut tokens);

        if tokens.has_more() {
            dbg!("No fully matched");
            return None;
        }

        if res {
            return Some(MatcherResult {});
        }

        None
    }

    fn matches_(&self, value: &AstValue, tokens: &mut IterableVec<InputToken>) -> bool {
        match value {
            AstValue::Terminal(t) => {
                match tokens.look_next() {
                    Some(InputToken::Word(word)) if word == t => {
                        tokens.move_next();
                        true
                    }
                    // TODO :: Not matched diagnostics error
                    _ => false,
                }
            }
            AstValue::NonTerminalRef(non_terminal_ref) => {
                let nt = self.find_nonterminal(non_terminal_ref);
                self.matches_(&nt.value, tokens)
            }
            AstValue::Serial(items) => {
                let mut iterator = items.iter();

                while let Some(next) = iterator.next() {
                    let valid_path = self.matches_(next, tokens);
                    if !valid_path {
                        return false;
                    }
                }

                true
            }
            AstValue::Choice(options) => options.iter().any(|e| self.matches_(e, tokens)),
            AstValue::Optional(stuff) => {
                let _matched_token = self.matches_(&stuff, tokens);
                true
            }
        }
    }

    fn find_nonterminal(&self, name: &str) -> &NonTerminal {
        self.non_terminals
            .iter()
            .find(|e| e.name == name)
            .unwrap_or_else(|| panic!("Expected to find nonterminal: {}", name))
    }
}

#[derive(Debug, Clone)]
enum DiagnosticError {
    // InifineLoop(AstItem),
    InifineLoop(String),

    UnknownNonTerminal(String),

    MultipleDeclaration(String),

    /// Or unreachable from root expression
    Unused(NonTerminal),
}

/// TODO :: Work with Positioned<T> items || For Editor support
pub fn ast_to_into_matcher(
    vec: Vec<AstItem>,
) -> Result<(Matcher, DiagnosticErrorContainer), DiagnosticErrorContainer> {
    let mut diagnostics_container = DiagnosticErrorContainer::default();

    let mut matcher_non_terminals: Vec<NonTerminal> = vec![];
    for e in vec.into_iter() {
        match e {
            AstItem::NonTerminalDeclaration(nt) => {
                if !matcher_non_terminals.contains(&nt) {
                    matcher_non_terminals.push(nt);
                } else {
                    diagnostics_container.add_error(DiagnosticError::MultipleDeclaration(nt.name));
                }
            }
            _ => {
                panic!("Unexpected AST item");
            }
        };
    }

    // TODO :: Branch analysis && Filter out unreachable branches !!
    let Some(first) = matcher_non_terminals.first() else {
        dbg!(matcher_non_terminals);
        return Err(diagnostics_container);
        // panic!("No value");
        // return Ok();
    };

    let mut reachable_non_terminals = vec![];
    let non_terminal_fetcher = NonTerminalFetcher {
        res: matcher_non_terminals.clone(),
    };

    find_reachable_expressions(
        &first.value,
        &mut reachable_non_terminals,
        &non_terminal_fetcher,
        &mut diagnostics_container,
    );

    for nonterminal in matcher_non_terminals.iter() {
        let reachable =
            nonterminal.name == first.name || reachable_non_terminals.contains(&nonterminal.name);
        if !reachable {
            diagnostics_container.add_warning(DiagnosticError::Unused(nonterminal.clone()))
        }
    }

    let mut state = ResolvabilityAnalyzerState::default();
    state.add_started(first.name.clone());

    // TODO :: Mark somehow the cycle
    if !resolvable_analysis(&first.value, &mut state, &non_terminal_fetcher) {
        diagnostics_container.add_error(DiagnosticError::InifineLoop("Sucka".into()));
    }

    if diagnostics_container.has_error {
        return Err(diagnostics_container);
    }

    Ok((
        Matcher::new(first.clone(), matcher_non_terminals),
        diagnostics_container,
    ))
}

fn resolvable_analysis(
    value: &AstValue,
    state: &mut ResolvabilityAnalyzerState,
    fetcher: &NonTerminalFetcher,
) -> bool {
    match value {
        AstValue::Terminal(_) => true,
        AstValue::NonTerminalRef(nt) => {
            if state.already_resolved(nt) {
                return true;
            }
            if state.is_started(nt) {
                return false;
            }
            if let Some(nonterminal) = fetcher.fetch(nt) {
                state.add_started(nt.clone());
                let res = resolvable_analysis(&nonterminal.value, state, fetcher);
                if res {
                    state.add_resolved(nt);
                }

                res
            } else {
                dbg!("Unresolvable nonterminal: {}", nt);
                false
            }
        }
        AstValue::Serial(s) => s.iter().all(|e| resolvable_analysis(e, state, fetcher)),
        AstValue::Choice(s) => s.iter().any(|e| resolvable_analysis(e, state, fetcher)),
        AstValue::Optional(_) => true,
    }
}

#[derive(Debug, Default)]
struct ResolvabilityAnalyzerState {
    started_non_terminals: Vec<String>,
    resolvable: Vec<String>,
}

impl ResolvabilityAnalyzerState {
    pub fn add_started(&mut self, nt: String) {
        self.started_non_terminals.push(nt);
    }

    pub fn already_resolved(&mut self, nt: &String) -> bool {
        self.resolvable.contains(nt)
    }

    pub fn is_started(&mut self, nt: &String) -> bool {
        self.started_non_terminals.contains(nt)
    }

    fn add_resolved(&mut self, nt: &str) -> () {
        // TODO : remove from started_nonterminals: self.started_terminals.
        self.resolvable.push(nt.into());
    }
}

#[derive(Debug)]
struct NonTerminalFetcher {
    res: Vec<NonTerminal>,
}

impl NonTerminalFetcher {
    pub fn fetch(&self, name: &str) -> Option<&NonTerminal> {
        self.res.iter().find(|e| e.name == name)
    }
}

#[derive(Debug, Clone, Default)]
pub struct DiagnosticErrorContainer {
    errors: Vec<DiagnosticError>,
    has_error: bool,
}

impl DiagnosticErrorContainer {
    pub fn add_error(&mut self, error: DiagnosticError) {
        self.errors.push(error);
        self.has_error = true
    }

    pub fn add_warning(&mut self, error: DiagnosticError) {
        self.errors.push(error);
    }
}

pub fn find_reachable_expressions(
    value: &AstValue,
    nonterminals: &mut Vec<String>,
    fetcher: &NonTerminalFetcher,
    diag_errors: &mut DiagnosticErrorContainer,
) {
    match value {
        AstValue::Terminal(_) => (),
        AstValue::NonTerminalRef(name) => {
            if nonterminals.contains(name) {
                return;
            };
            nonterminals.push(name.clone());
            if let Some(token) = fetcher.fetch(name) {
                let rv =
                    find_reachable_expressions(&token.value, nonterminals, fetcher, diag_errors);
                //     if rv { nonterminals.push(name.clone())
                // }
                rv
            } else {
                diag_errors.add_error(DiagnosticError::UnknownNonTerminal(name.clone()));
                // panic!("Unrecognised unrecognised name: <{}>", name);
            }
        }
        AstValue::Choice(s) | AstValue::Serial(s) => s
            .iter()
            .for_each(|e| find_reachable_expressions(e, nonterminals, fetcher, diag_errors)),
        AstValue::Optional(s) => find_reachable_expressions(s, nonterminals, fetcher, diag_errors),
    }
}

#[cfg(test)]
mod analyzer_tests {

    use crate::parser::parse_text;
    use crate::utils::ErrorUnwrap;

    use super::*;
    #[test]
    fn bad_analyzer_test_0() {
        let s = &read_test_file("/negative_test_0.hbnf");
        let tokens = parse_text(s).expect("Unable to parse :(");
        dbg!(&tokens);
        ast_to_into_matcher(tokens).expect_err("Unable to analyze");
    }

    #[test]
    fn bad_analyzer_test_1() {
        let s = &read_test_file("/negative_test_1.hbnf");
        let tokens = parse_text(s).expect("Unable to parse :(");

        dbg!(&tokens);
        let res = ast_to_into_matcher(tokens).expect_err("Succsessful, but error expected");
        dbg!(res);
    }

    fn get_test_file(s: &str) -> String {
        env!("TEST_FOLDER").to_owned() + s
    }

    fn read_test_file(s: &str) -> String {
        let res = get_test_file(s);
        std::fs::read_to_string(res).unwrap()
    }

    #[test]
    fn unresolvable_analysis() {
        let res = std::fs::read_dir(get_test_file("/unresolvable")).expect("Folder not readable");
        for file_res in res {
            let file = file_res.expect("Unable to read file");
            dbg!(&file);
            let file_content = std::fs::read_to_string(file.path()).expect("Unable to read file");

            let tokens = parse_text(&file_content)
                .unwrap_or_else(|| panic!("Unable to parse file :(\n{}", &file_content));

            let _res = ast_to_into_matcher(tokens).expect_err(&format!(
                "\n\nSuccsessful, but error expected error for file:\n=====\n{}\n===== ",
                &file_content
            ));
            // res.
        }
    }

    #[test]
    fn test_grammar() {
        let grammar = r#"
<expression> :: <num>

<num> :: "0" | "1"
<operand> :: "&&" | "||"
<unary_operand> :: "!"
"#
        .trim();

        let tokens = parse_text(grammar).expect("Unable to parse :(");
        let res = ast_to_into_matcher(tokens)
            .map_error(|e| panic!("Unable to parse file: {}\n======\n{:?}", grammar, e));
    }
}

#[cfg(test)]
mod grammar_test {
    use core::panic;

    use crate::utils::ErrorUnwrap;

    use super::{ast_to_into_matcher, DiagnosticErrorContainer, Matcher};

    #[derive(Debug, Clone)]
    struct GrammarTest {
        grammer_text: String,
        test_cases: Vec<TestCase>,
        diagnostics: DiagnosticErrorContainer,
        matcher: Matcher,
    }

    #[derive(Debug, Clone)]
    struct TestCase {
        name: String,
        test_text: String,
        // meta_data: Vec<String>, // Parser from name @data @ignored @fail @pass
    }

    impl TestCase {
        fn new(name: String, test_text: String) -> Self {
            Self { name, test_text }
        }
    }

    #[derive(Debug, Default, Clone)]
    struct GrammarTextFile {
        grammar_tests: Vec<GrammarTest>,
    }

    impl GrammarTextFile {
        fn new(grammars: Vec<GrammarTest>) -> Self {
            Self {
                grammar_tests: grammars,
            }
        }
    }

    fn get_test_file(s: &str) -> String {
        let file = env!("TEST_FOLDER").to_owned() + "/" + s;
        std::fs::read_to_string(&file).unwrap_or_else(|_| panic!("Unable to read file: {}", file))
    }

    fn g_text_parser(test_file: &str) -> GrammarTextFile {
        let file = get_test_file(test_file);
        let parts = LineSplitter::new(file.lines(), "=============");

        let grammar_tests: Vec<GrammarTest> = parts
            .map(|item| {
                const TEST_CASE_NAME_SPLIT_POINT: usize = "#######".len() + 1;
                let mut iterator = LineSplitter::new(item.lines(), "#######");

                let item = iterator.next().expect("Missing grammar");

                let mut test_cases: Vec<TestCase> = Vec::<TestCase>::new();

                let mut test_header = iterator.last_split();

                while let Some(next_item) = iterator.next() {
                    let test_case_str = next_item.trim();
                    let test_case_name = test_header.split_off(TEST_CASE_NAME_SPLIT_POINT);

                    test_header = iterator.last_split();

                    test_cases.push(TestCase::new(test_case_name, test_case_str.into()));
                }

                let grammer_text = item;
                let parsed = crate::parser::parse_text(&grammer_text)
                    .unwrap_or_else(|| panic!("Unable to parse grammar: \n{}", grammer_text));
                let (matcher, diagnostics) = ast_to_into_matcher(parsed)
                    .map_error(|e| panic!("Error withing the grammar:\n{}\n{:?}", grammer_text, e));

                GrammarTest {
                    grammer_text,
                    diagnostics,
                    matcher,
                    test_cases,
                }
            })
            .collect();

        GrammarTextFile::new(grammar_tests)
    }

    // fn execute_test_case(testCase: &TestCase) {}

    struct LineSplitter<'a> {
        lines: core::str::Lines<'a>,
        splitter: &'static str,
        split_chars: Vec<String>,
    }

    impl<'a> LineSplitter<'a> {
        fn new(lines: core::str::Lines<'a>, splitter: &'static str) -> Self {
            Self {
                lines,
                splitter,
                split_chars: vec![],
            }
        }

        fn get_last_split(&self) -> Option<&String> {
            return self.split_chars.last();
        }

        fn last_split(&self) -> String {
            return self.get_last_split().unwrap().clone();
        }
    }

    impl Iterator for LineSplitter<'_> {
        type Item = String;

        fn next(&mut self) -> Option<Self::Item> {
            let mut str_agg = String::new();

            while let Some(read_value) = self.lines.next() {
                if read_value.contains(self.splitter) {
                    self.split_chars.push(read_value.into());
                    let to_return = str_agg;
                    str_agg = String::new();
                    return Some(to_return);
                }
                if !str_agg.is_empty() {
                    str_agg.push('\n'); // Windows support ??
                }

                str_agg.push_str(read_value);
            }

            if str_agg.is_empty() {
                None
            } else {
                Some(str_agg)
            }
        }
    }

    struct Assertor<'a> {
        grammar: &'a GrammarTest,
        testcase: &'a TestCase,
    }

    impl<'a> Assertor<'a> {
        pub fn new(grammar: &'a GrammarTest, testcase: &'a TestCase) -> Self {
            Self { grammar, testcase }
        }

        pub fn assert(&self, is_valid: bool) {
            self.assert_named(is_valid, "Assertion falied");
        }

        pub fn assert_named(&self, is_valid: bool, x: &str) {
            if !is_valid {
                panic!(
                    "||||||||||||||||||||||||| Grammar:\n\n{}\n\n####### Test file:\n{}\n=============\n\tError: {}\n",
                    self.grammar.grammer_text,
                    self.testcase.test_text,
                    x
                );
            }
        }
    }

    #[test]
    fn sanity_checks() {
        let file = r#"
1
####
2
#### But better
3
####
        "#
        .trim();
        let mut iterator = LineSplitter::new(file.lines(), "####");
        assert_eq!(iterator.next(), Some("1".into()));
        assert_eq!(iterator.last_split(), "####");
        assert_eq!(iterator.next(), Some("2".into()));
        assert_eq!(iterator.last_split(), "#### But better");
        assert_eq!(iterator.next(), Some("3".into()));
        assert_eq!(iterator.last_split(), "####");
        assert_eq!(iterator.next(), None);
    }

    #[test]
    fn run_tests() {
        // let only = Some("Optional first");

        let file = g_text_parser("grammer/text_exmaple.grammar.test");
        for test in file.grammar_tests {
            for testcase in test.test_cases.iter() {
                let assertor = Assertor::new(&test, &testcase);
                let skip = testcase.name.contains("@skip");

                if skip {
                    println!("> [Warning] Skipping testcase:\n{:?}", testcase);
                    continue;
                }

                let negative_test = testcase.name.contains("@invalid");
                let stripped = testcase.test_text.trim();

                let res = test.matcher.matches(stripped);

                if negative_test {
                    assertor.assert_named(res.is_none(), "Expected to not match");
                    continue;
                }

                assertor.assert_named(res.is_some(), "Expected to match");
            }
        }
    }
}
