#[derive(Debug, Clone, PartialEq)]
pub enum InputToken {
    Word(String),
}

pub struct InputTokeniser;

impl InputTokeniser {
    pub fn tokenise(&self, text: &str) -> Vec<InputToken> {
        text.split_whitespace()
            .map(|e| InputToken::Word(e.into()))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use crate::input_tokeniser::{InputToken, InputTokeniser};

    #[test]
    fn test_input_tokeniser() {
        let t = InputTokeniser;
        let test = "asd asd";
        let res = t.tokenise(test);

        assert_eq!(
            res,
            vec![
                InputToken::Word("asd".into()),
                InputToken::Word("asd".into()),
            ]
        );

        let t = InputTokeniser;
        let test = "asd   asd";
        let res = t.tokenise(test);
        assert_eq!(
            res,
            vec![
                InputToken::Word("asd".into()),
                InputToken::Word("asd".into()),
            ]
        );
    }
}
