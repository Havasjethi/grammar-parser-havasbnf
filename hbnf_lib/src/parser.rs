use sql_parser::sql_recommendation::iterable_vec::IterableVec;

use crate::{
    tokeniser::{tokenise, TokenWithPos},
    types::*,
};

pub fn parse_text(content: &str) -> Option<Vec<AstItem>> {
    // TODO :: Move this to parser.rs
    let res = tokenise(content);
    let mut tokens = IterableVec::from(res);

    let mut items: Vec<AstItem> = Vec::new();

    while let Some(token) = tokens.get_next_token() {
        match token {
            Token::NonTerminal(name) => {
                dbg!("Parisng nonTermianl");
                tokens.validate_next_token(Token::DoubleColon);
                if let Some(value) = parse_ast_value(&mut tokens) {
                    let nt = NonTerminal { name, value };
                    items.push(AstItem::NonTerminalDeclaration(nt));
                } else {
                    dbg!("Non terminal not found");
                }
            }
            e => panic!("Unrecognised token: {e:?} \nStuff: {:?}", &items),
        }
    }

    Some(items)
}

pub fn parse_ast_value(tokens: &mut IterableVec<TokenWithPos>) -> Option<AstValue> {
    let mut choices: Vec<AstValue> = Vec::new();
    let mut values: Vec<AstValue> = Vec::new();

    while let Some(token) = tokens.look_next() {
        match &token.1 {
            Token::ParenthesesOpen => {
                panic!("Unsupported");
                tokens.move_next();
                if let Some(v) = parse_ast_value(tokens) {
                    values.push(AstValue::Optional(Box::new(v)));
                } else {
                    dbg!("Unable to parse (... )");
                    return None;
                }
            }
            Token::AngleLeft => {
                tokens.move_next();
                if let Some(v) = parse_ast_value(tokens) {
                    values.push(AstValue::Optional(Box::new(v)));
                } else {
                    dbg!("Unable to parse Optional ");
                    return None;
                }
                // Token::AngleRight => todo!(),
            }
            Token::AngleRight => {
                // Basic flow stuff
                return collapse(choices, values);
            }
            Token::NonTerminal(t) => {
                match tokens.look_forward(2) {
                    Some(TokenWithPos(_, Token::DoubleColon)) => break,
                    _ => {}
                }
                values.push(AstValue::NonTerminalRef(t.clone()))
            }
            Token::Word(t) => values.push(AstValue::Terminal(t.clone())),
            Token::Choose => {
                let value_to_push = collapse(Vec::new(), values)?;
                choices.push(value_to_push);
                values = Vec::new();
                // TODO :: Trailing choice is an option
            }
            _ => panic!("Unexpected token: {:?}", token),
        }

        tokens.move_next();
    }

    collapse(choices, values)
}

pub trait AA {
    fn get_next_token(&mut self) -> Option<Token>;
    fn look_next_token(&self) -> Option<Token>;
    fn validate_next_token(&mut self, t: Token);
}

impl AA for IterableVec<TokenWithPos> {
    fn get_next_token(&mut self) -> Option<Token> {
        self.get_some_next().map(|e| e.1)
    }

    fn look_next_token(&self) -> Option<Token> {
        self.look_at(self.get_index()).map(|e| e.1.clone())
    }

    fn validate_next_token(&mut self, t: Token) {
        assert!(self.get_next_token().unwrap() == t);
    }
}

fn collapse(mut choices: Vec<AstValue>, mut values: Vec<AstValue>) -> Option<AstValue> {
    dbg!("Collapse called", &choices, &values);
    // (1, 1) => Not important, The important part is whether choices is empty or not
    match (choices.len(), values.len()) {
        (0, 0) => None,
        (0, 1) => Some(values.pop().unwrap()),
        (0, _x) => Some(AstValue::Serial(values)),
        (1, 0) => Some(choices.pop().unwrap()),
        (choice_count, 1) => {
            dbg!(choice_count);
            choices.push(values.pop().unwrap());
            Some(AstValue::Choice(choices))
        }
        (_choice_count, _x) => {
            choices.push(AstValue::Serial(values));
            Some(AstValue::Choice(choices))
        }
    }
}

#[cfg(test)]
mod parser_test {
    use super::*;

    #[test]
    fn test_0() {
        let ast_stuff = parse_text(include_str!("../../test_files/test_0.hbnf"));
        assert!(ast_stuff.is_some());
    }

    #[test]
    fn test_1() {
        let ast_stuff =
            parse_text(include_str!("../../test_files/test_1.hbnf")).expect("Unable to parse");
        assert!(
            ast_stuff[0]
                == AstItem::NonTerminalDeclaration(NonTerminal {
                    name: "expression".into(),
                    value: AstValue::NonTerminalRef("num".into()),
                })
        );

        dbg!(&ast_stuff[1]);

        assert!(
            ast_stuff[1]
                == AstItem::NonTerminalDeclaration(NonTerminal {
                    name: "num".into(),
                    value: AstValue::Choice(vec![
                        AstValue::Terminal("0".into()),
                        AstValue::Terminal("1".into()),
                    ],),
                })
        );
        // ast_stuff == NonTerminalDeclaration
        // dbg!(ast_stuff);
    }

    #[test]
    fn test_2() {
        let ast_stuff = parse_text(include_str!("../../test_files/test_2.hbnf"));
        assert!(ast_stuff.is_some());
    }
}
