use sql_parser::sql_recommendation::iterable_vec::IterableVec;

use crate::types::*;

#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct Pos(pub u32, pub u32);

#[derive(Default, Debug)]
pub struct ProcessContext {
    // aggregated_items: String, // TODO :: Remove this for optimizations
    // is_symbol_array: String,
    pub tokens: Vec<TokenWithPos>,
    pub line_number: u32,
    pub col_number: u32,
    // bytes_offset: u64,
    pub aggretate_start: Pos,
}

impl ProcessContext {
    pub fn get_pos(&mut self) -> Pos {
        Pos(self.line_number, self.col_number)
    }

    pub fn new_line(&mut self) {
        self.line_number += 1;
        self.col_number = 0;
    }

    pub fn step(&mut self) {
        self.col_number += 1;
    }

    pub fn add_token(&mut self, t: Token) {
        let pos = self.get_pos();
        self.tokens.push(TokenWithPos(pos, t));
    }

    pub fn finish_process(self) -> Vec<TokenWithPos> {
        self.tokens
    }
}

#[derive(Debug, Clone)]
pub struct TokenWithPos(pub Pos, pub Token);

pub fn tokenise(content: &str) -> Vec<TokenWithPos> {
    let mut context = ProcessContext::default();

    use Token::*;

    let chars = content.chars();
    let mut iterator = chars.into_iter();

    while let Some(x) = iterator.next() {
        match x {
            '#' => {
                for next in iterator.by_ref() {
                    if next == '\n' {
                        break;
                    }
                }
            }
            '(' => context.add_token(ParenthesesOpen),
            ')' => context.add_token(ParenthesesClone),
            '[' => context.add_token(AngleLeft),
            ']' => context.add_token(AngleRight),
            '<' => {
                let mut word = String::new();
                for inner in iterator.by_ref() {
                    if inner == '>' {
                        break;
                    }
                    word.push(inner);
                }
                context.add_token(NonTerminal(word));
            }
            '|' => context.add_token(Choose),
            ':' => match iterator.next() {
                Some(':') => context.add_token(DoubleColon),
                _ => panic!("Bad stuff"),
            },
            '"' => {
                let mut word = String::new();
                for inner in iterator.by_ref() {
                    if inner == '"' {
                        break;
                    }
                    word.push(inner);
                }
                context.add_token(Word(word));
            }
            '\n' => context.new_line(),
            ' ' | '\t' => {}
            t => {
                println!("Unrecognised char: {t}")
            }
        };

        context.step();
    }

    context.finish_process()
}
