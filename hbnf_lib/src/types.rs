#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Token {
    AngleLeft,
    AngleRight,
    ParenthesesOpen,
    ParenthesesClone,
    /// < name >
    NonTerminal(String),

    /// " name "
    Word(String),
    Choose,
    DoubleColon,
    // WhiteSpace,
}

#[derive(Debug, Clone, PartialEq)]
pub enum AstItem {
    NonTerminalDeclaration(NonTerminal),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum AstValue {
    Terminal(String),
    NonTerminalRef(String),
    Serial(Vec<AstValue>),
    Choice(Vec<AstValue>),
    Optional(Box<AstValue>),
}

#[derive(Debug, Clone, PartialEq)]
pub struct NonTerminal {
    pub name: String,
    // choices: Vec<AstValue>,
    pub value: AstValue,
}

impl NonTerminal {
    fn same_name(&self, other: &Self) -> bool {
        self.name == other.name
    }
}
