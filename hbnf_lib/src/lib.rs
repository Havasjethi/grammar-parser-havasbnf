pub mod matcher;
pub mod tokeniser;
pub mod types;

mod input_tokeniser;
pub mod parser;

pub mod utils {
    pub trait ErrorUnwrap<T, E> {
        fn map_error<F>(self, error_handler: F) -> T
        where
            F: FnOnce(E) -> ();
    }

    impl<T, E> ErrorUnwrap<T, E> for Result<T, E> {
        fn map_error<F>(self, error_handler: F) -> T
        where
            F: FnOnce(E) -> (),
        {
            match self {
                Ok(v) => v,
                Err(e) => {
                    error_handler(e);
                    panic!("Method did not throw.")
                }
            }
        }
    }
}
