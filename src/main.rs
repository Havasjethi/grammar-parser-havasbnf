use generic_parser_lib::parser::parse_text;

fn main() {
    let ast_stuff = parse_text(include_str!("../test_files/test_0.hbnf"));
    dbg!(ast_stuff);
}
